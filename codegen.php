<?php
/**
 *
 * Member Invite Code Generation
 *
 * @copyright (c) 2018 Paul Unterberg
 * @license MIT License 
 * ===========================
 * Meant to be included into PHPBB modules and process variables $userId  and $targetEmail = '';
 * ===========================
 */

function generateRandomNum($length = 3) {
    return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
}


function generateRandomString($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


function generateCode($userId, $targetEmail)

{

include '/config.php';
$h = $dbhost;
$u = $dbuser;
$p = $dbpasswd;
$n = $dbname;


/*
Check to see if a user has generated a code in the last 30 days
*/

// Create connection
$conn = new mysqli($h, $u, $p, $n);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//run Query
$qry = "select Count(*) as cnt from phpbb_usercode where userid = '".$userId."' and datetime BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW();";

$result = $conn->query($qry);


    // output data of each row
   $row = $result->fetch_assoc();
  $codeCount = $row["cnt"];

  if ($codeCount > 0) 
  {
  	//User has too many codes in 30 days.
  	$conn->close();
  	return "2";
  }
  else
      {
         //User hasn't generated any codes
      
    // generate random code 
    $randCode = generateRandomString(15);

    //Mark code into DB
	$qry2 = "INSERT INTO  phpbb_usercode VALUES (NULL, '".$userId."',  '".$randCode."', NOW( ) ,  '0',  '".$targetEmail."'
	);";


	$result2 = $conn->query($qry2);
	$conn->close();

  $to      = $targetEmail;
  $subject = 'You have been invited to join GalaxyRaver!';
  $message = 'Hello! Use the code:  ' . $randCode . ' when you are on the registration page.' ;
  $headers = 'From: webmaster@galaxyraver.com' . "\r\n" .
    'Reply-To: webmaster@galaxyraver.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);

	//success!
  	return "1";

    }

}
/*
$rndEm= generateRandomString(12) . "@example.com";
$rNum = generateRandomNum();
generateCode($rNum,$rndEm);
*/

//validateCode('VuhhepD7ifN08Ew','wZv9iDGTWrxE@example.com');
$targetEmail = htmlspecialchars($_POST["target"]);
$uid = htmlspecialchars($_POST["uid"]);
//echo "User: " . $uid . " : Em: " . $targetEmail;

$cGen = generateCode($uid,$targetEmail);

if ($cGen == 1)
{
    //echo "{status: 200}";
  // header("Location: codestat.php?status=200");
// Specify the path to your phpBB3 installation directory.
	define('IN_PHPBB', true);
// Specify the path to your phpBB3 installation directory.
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
// The common.php file is required.
include($phpbb_root_path . 'common.' . $phpEx);

// since we are grabbing the user avatar, the function is inside the functions_display.php file since RC7
include($phpbb_root_path . 'includes/functions_display.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);

// specify styles and/or localisation
// in this example, we specify that we will be using the file: my_language_file.php
$user->setup('mods/my_language_file');

/*
* All of your coding will be here, setting up vars, database selects, inserts, etc...
*
* This is a very primitive example, it’s meant to show you a working example only.
*/

$example_variable = sprintf($user->lang['TIME_NOW'], $user->format_date(time()));
//$google_logo = '<a href="http://www.google.com/"><img src="http://www.google.com/intl/en_ALL/images/logo.gif" alt="Google" /></a>';

// Set the filename of the template you want to use for this file.
// This is the name of our template file located in /styles/<style>/templates/.



   //echo 'Thanks for logging in, ' . $user->data['username_clean'];
    $template->set_filenames(array(
    'body' => 'send200.html',
	));




// Page title, this language variable should be defined in the language file you setup at the top of this page.
page_header($user->lang['MY_TITLE']);



// Completing the script and displaying the page.
page_footer();



}
else
{
 //echo "{status: 200}";
  // header("Location: codestat.php?status=200");
// Specify the path to your phpBB3 installation directory.
	define('IN_PHPBB', true);
// Specify the path to your phpBB3 installation directory.
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
// The common.php file is required.
include($phpbb_root_path . 'common.' . $phpEx);

// since we are grabbing the user avatar, the function is inside the functions_display.php file since RC7
include($phpbb_root_path . 'includes/functions_display.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);

// specify styles and/or localisation
// in this example, we specify that we will be using the file: my_language_file.php
$user->setup('mods/my_language_file');

/*
* All of your coding will be here, setting up vars, database selects, inserts, etc...
*
* This is a very primitive example, it’s meant to show you a working example only.
*/

$example_variable = sprintf($user->lang['TIME_NOW'], $user->format_date(time()));
//$google_logo = '<a href="http://www.google.com/"><img src="http://www.google.com/intl/en_ALL/images/logo.gif" alt="Google" /></a>';

// Set the filename of the template you want to use for this file.
// This is the name of our template file located in /styles/<style>/templates/.



   //echo 'Thanks for logging in, ' . $user->data['username_clean'];
    $template->set_filenames(array(
    'body' => 'send500.html',
	));




// Page title, this language variable should be defined in the language file you setup at the top of this page.
page_header($user->lang['MY_TITLE']);



// Completing the script and displaying the page.
page_footer();
}

?>