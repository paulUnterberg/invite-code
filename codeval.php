<?php
/**
 *
 * Member Invite Code Generation
 *
 * @copyright (c) 2018 Paul Unterberg
 * @license MIT License 
 * ===========================
 * Meant to be included into PHPBB modules and process variables $userId  and $targetEmail = '';
 * ===========================
 */

function generateRandomNum($length = 3) {
    return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
}


function generateRandomString($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


function generateCode($userId, $targetEmail)

{

include '/config.php';
$h = $dbhost;
$u = $dbuser;
$p = $dbpasswd;
$n = $dbname;


/*
Check to see if a user has generated a code in the last 30 days
*/

// Create connection
$conn = new mysqli($h, $u, $p, $n);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//run Query
$qry = "select Count(*) as cnt from phpbb_usercode where userid = '".$userId."' and datetime BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW();";

$result = $conn->query($qry);


    // output data of each row
   $row = $result->fetch_assoc();
  $codeCount = $row["cnt"];

  if ($codeCount > 1) 
  {
    //User has too many codes in 30 days.
    $conn->close();
    return "2";
  }
  else
      {
         //User hasn't generated any codes
      
    // generate random code 
    $randCode = generateRandomString(15);

    //Mark code into DB
  $qry2 = "INSERT INTO  phpbb_usercode VALUES (NULL, '".$userId."',  '".$randCode."', NOW( ) ,  '0',  '".$targetEmail."'
  );";


  $result2 = $conn->query($qry2);
  $conn->close();

  //success!
    return "1";

    }

}
/*
$rndEm= generateRandomString(12) . "@example.com";
$rNum = generateRandomNum();
generateCode($rNum,$rndEm);
*/

//validateCode('VuhhepD7ifN08Ew','wZv9iDGTWrxE@example.com');


?>